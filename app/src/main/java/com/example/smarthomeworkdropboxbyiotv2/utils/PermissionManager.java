package com.example.smarthomeworkdropboxbyiotv2.utils;

import android.content.IntentFilter;

import com.example.smarthomeworkdropboxbyiotv2.iotservice.UsbService;

public class PermissionManager {

    public IntentFilter setFilters() {
        IntentFilter filter = new IntentFilter();
        filter.addAction(UsbService.ACTION_USB_PERMISSION_GRANTED);
        filter.addAction(UsbService.ACTION_NO_USB);
        filter.addAction(UsbService.ACTION_USB_DISCONNECTED);
        filter.addAction(UsbService.ACTION_USB_NOT_SUPPORTED);
        filter.addAction(UsbService.ACTION_USB_PERMISSION_NOT_GRANTED);
        return filter;
    }
}
