package com.example.smarthomeworkdropboxbyiotv2.activity;

import androidx.appcompat.app.AppCompatActivity;

import android.content.BroadcastReceiver;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.ServiceConnection;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.media.MediaPlayer;
import android.os.Bundle;
import android.os.Handler;
import android.os.IBinder;
import android.os.Message;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.RadioButton;
import android.widget.TextView;
import android.widget.Toast;

import com.example.smarthomeworkdropboxbyiotv2.R;
import com.example.smarthomeworkdropboxbyiotv2.iotservice.UsbService;
import com.example.smarthomeworkdropboxbyiotv2.service.DatabaseStudent;
import com.example.smarthomeworkdropboxbyiotv2.utils.BroadcastManager;
import com.example.smarthomeworkdropboxbyiotv2.utils.PermissionManager;

import java.lang.ref.WeakReference;
import java.util.ArrayList;
import java.util.Set;
import java.util.Timer;
import java.util.TimerTask;

public class LoginQrcodeNoInternet extends AppCompatActivity {

    // Notifications from UsbService will be received here.
    private final BroadcastManager broadcastManager = new BroadcastManager();
    private final BroadcastReceiver mUsbReceiver = broadcastManager.createBroadcast();
    private final PermissionManager permissionManager = new PermissionManager();

    private UsbService usbService;
    TextView textViewUsbQr;

    // get serial port
    private MyHandler mHandler;
    String getDataUsb;

    TextView textContent, textViewIdStudent;

    String EmailHolder;
    Boolean checkEditText;

    String idStudent, idSubject, idBox;
    EditText editTextIdSubject;
    RadioButton radioButtonTh1;
    RadioButton radioButtonTh2;
    RadioButton radioButtonTh3;
    RadioButton radioButtonTh4;
    String radioButtonSend;
    String id2, id3;

    // set name select send homework
    DatabaseStudent mHelper;
    SQLiteDatabase mDb;
    Cursor mCursor;

    MediaPlayer media1, media2, media3;

    // send usb data
    private final ServiceConnection usbConnection = new ServiceConnection() {
        @Override
        public void onServiceConnected(ComponentName arg0, IBinder arg1) {
            usbService = ((UsbService.UsbBinder)arg1).getService();
            usbService.setHandler(mHandler); // get serial port
        }

        @Override
        public void onServiceDisconnected(ComponentName arg0) {
            usbService = null;
        }
    };
    // end usb

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login_qrcode_no_internet);

        mHandler = new MyHandler(this); // get serial port

        mHelper = new DatabaseStudent(this);
        mDb = mHelper.getWritableDatabase();
        setNameButton();

        textViewUsbQr = (TextView)findViewById(R.id.text_view_usb_qr);
        textContent = (TextView)findViewById(R.id.text_content);
        Button buttonOk = (Button)findViewById(R.id.btn_ok);
        Button buttonBack = (Button)findViewById(R.id.btn_back_main);

        textViewIdStudent = (TextView)findViewById(R.id.txt_id_student);

        editTextIdSubject = (EditText)findViewById(R.id.edit_id_subject);
        radioButtonTh1 = (RadioButton)findViewById(R.id.radio_th_1);
        radioButtonTh2 = (RadioButton)findViewById(R.id.radio_th_2);
        radioButtonTh3 = (RadioButton)findViewById(R.id.radio_th_3);
        radioButtonTh4 = (RadioButton)findViewById(R.id.radio_th_4);
        TextView textViewPass = (TextView)findViewById(R.id.txt_test_passwd);
        textViewPass.setText(radioButtonSend);

//        MediaPlayer howToScan = MediaPlayer.create(getApplicationContext(), R.raw.howtoscan);
//        howToScan.start();

        Intent intent = new Intent(getApplicationContext(), ScanActivity.class);
        startActivityForResult(intent, 999);

        media1 = MediaPlayer.create(getApplicationContext(), R.raw.homeloginscanqrcode);
        media2 = MediaPlayer.create(getApplicationContext(), R.raw.homeloginscanqrcode);
        media3 = MediaPlayer.create(getApplicationContext(), R.raw.boxfull);

        radioButtonTh1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                radioButtonSend = "T1";
                // send usb Data
                textViewUsbQr.setText("6");
                if (!textViewUsbQr.getText().toString().equals("")) {
                    String data = textViewUsbQr.getText().toString();
                    // if UsbService was correctly binded, Send data
                    if (usbService != null) {
                        usbService.write(data.getBytes());
                    }
                }
                // end
            }
        });

        radioButtonTh2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                radioButtonSend = "T2";
                // send usb Data
                textViewUsbQr.setText("7");
                if (!textViewUsbQr.getText().toString().equals("")) {
                    String data = textViewUsbQr.getText().toString();
                    // if UsbService was correctly binded, Send data
                    if (usbService != null) {
                        usbService.write(data.getBytes());
                    }
                }
                // end
            }
        });

        radioButtonTh3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                radioButtonSend = "T3";
                // send usb Data
                textViewUsbQr.setText("8");
                if (!textViewUsbQr.getText().toString().equals("")) {
                    String data = textViewUsbQr.getText().toString();
                    // if UsbService was correctly binded, Send data
                    if (usbService != null) {
                        usbService.write(data.getBytes());
                    }
                }
                // end
            }
        });

        radioButtonTh4.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                radioButtonSend = "T4";
                // send usb Data
                textViewUsbQr.setText("9");
                if (!textViewUsbQr.getText().toString().equals("")) {
                    String data = textViewUsbQr.getText().toString();
                    // if UsbService was correctly binded, Send data
                    if (usbService != null) {
                        usbService.write(data.getBytes());
                    }
                }
                // end
            }
        });

        ImageButton imageButtonSpeaker = (ImageButton)findViewById(R.id.img_btn_speaker);
        imageButtonSpeaker.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                media1.stop();
                media2.start();
            }
        });

        buttonOk.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                media1.stop();
                media2.stop();
                checkEditTextIsEmptyOrNot();
                if (checkEditText) {
                    Intent intent = new Intent(LoginQrcodeNoInternet.this, SendHomeWorkNoInternet.class);
                    intent.putExtra("ShowUser", EmailHolder);
                    //send usb data
                    textViewUsbQr.setText("5");
                    if (!textViewUsbQr.getText().toString().equals("")) {
                        String data = textViewUsbQr.getText().toString();
                        if (usbService != null) {
                            usbService.write(data.getBytes());
                        }
                    }
                    // end

                    startActivity(intent);
                    finish();
                } else {
                    Toast.makeText(getApplicationContext(), "ช่องว่าง กรุณากรอกข้อมูลให้เรียบร้อย", Toast.LENGTH_SHORT).show();
                }
            }
        });

        buttonBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                media1.stop();
                media2.stop();
                Intent intentMain = new Intent(getApplicationContext(), MainActivity.class);
                startActivity(intentMain);
                finish();
            }
        });
    }

    public void onActivityResult(int requestCode, int resultCode, Intent intent) {
        super.onActivityResult(requestCode, resultCode, intent);

        if (resultCode == RESULT_OK) {
            String contents = intent.getStringExtra("SCAN_RESULT");
            textContent.setText(contents);

            media1.start();

            String idQrCodeScan = textContent.getText().toString();
            try {
                String id1, id2, id3;
                id1 = idQrCodeScan.split("-")[2];
                id2 = idQrCodeScan.split("-")[3];
                id3 = idQrCodeScan.split("-")[1];
                idStudent = id1 + "-" + id2;
                textViewIdStudent.setText(idStudent);
                editTextIdSubject.setText(id3);
            } catch (Exception e) {
                textViewIdStudent.setText(idQrCodeScan);
                idStudent = idQrCodeScan;
            }
        } else {
            Intent intentBackError = new Intent(getApplicationContext(), MainActivity.class);
            startActivity(intentBackError);
            finish();
        }
    }

    // เช็ตว่าช่องใส่ข้อมูลว่างหรือไม่
    public void checkEditTextIsEmptyOrNot() {
        id2 = editTextIdSubject.getText().toString();
        id3 = radioButtonSend + "-" + id2 + "-" + idStudent;
        EmailHolder = id3;
        if (TextUtils.isEmpty(EmailHolder)) {
            checkEditText = false;
        } else {
            checkEditText = true;
        }
    }

    // send usb data
    @Override
    public void onResume() {
        super.onResume();
        registerReceiver(mUsbReceiver, permissionManager.setFilters());
        startService(UsbService.class, usbConnection, null);
    }

    @Override
    public void onPause() {
        super.onPause();
        unregisterReceiver(mUsbReceiver);
        unbindService(usbConnection);
    }

    private void startService(Class<?> service, ServiceConnection serviceConnection, Bundle extras) {
        if (!UsbService.SERVICE_CONNECTED) {
            Intent startService = new Intent(this, service);
            if (extras != null && !extras.isEmpty()) {
                Set<String> keys = extras.keySet();
                for (String key : keys) {
                    String extra = extras.getString(key);
                    startService.putExtra(key, extra);
                }
            }
            startService(startService);
        }
        Intent bindingIntent = new Intent(this, service);
        bindService(bindingIntent, serviceConnection, Context.BIND_AUTO_CREATE);
    }
    // end send usb data

    public void setNameButton() {
        mCursor = mDb.rawQuery("SELECT * FROM " + DatabaseStudent.TABLE_NAME_BUTTON, null);
        ArrayList<String> arr_list = new ArrayList<String>();
        mCursor.moveToFirst();
        while (!mCursor.isAfterLast()) {
            String valStd = mCursor.getString(mCursor.getColumnIndex(DatabaseStudent.COL_BTNAME));
            arr_list.add(valStd);
            mCursor.moveToNext();
        }

        radioButtonTh1 = (RadioButton)findViewById(R.id.radio_th_1);
        radioButtonTh2 = (RadioButton)findViewById(R.id.radio_th_2);
        radioButtonTh3 = (RadioButton)findViewById(R.id.radio_th_3);
        radioButtonTh4 = (RadioButton)findViewById(R.id.radio_th_4);

        try {
            radioButtonTh1.setText(arr_list.get(0));
            radioButtonTh2.setText(arr_list.get(1));
            radioButtonTh3.setText(arr_list.get(2));
            radioButtonTh4.setText(arr_list.get(3));
        } catch (Exception e) {
            Toast.makeText(getApplicationContext(), "ไม่มีชื่อ Button ใน DB", Toast.LENGTH_SHORT).show();
        }
    }

    // get data usb
    private class MyHandler extends Handler {
        private final WeakReference<LoginQrcodeNoInternet> mActivity;

        public MyHandler(LoginQrcodeNoInternet activity) {
            mActivity = new WeakReference<>(activity);
        }

        @Override
        public void handleMessage(Message msg) {
            switch (msg.what) {
                case UsbService.MESSAGE_FROM_SERIAL_PORT:
                    String data = (String) msg.obj;
                    getDataUsb = data; // get data usb to getDataUsb
                    //Toast.makeText(getApplicationContext(), getDataUsb, Toast.LENGTH_SHORT).show();
                    if (getDataUsb.equals("1")) {
                        media1.stop();
                        media2.stop();
                        media3.start();
                        Toast.makeText(getApplicationContext(), "ขออภัยการบ้านกล่อง 1 เต็ม", Toast.LENGTH_SHORT).show();
                        final Handler handler = new Handler();
                        handler.postDelayed(new Runnable() {
                            @Override
                            public void run() {
                                Intent intentFull = new Intent(getApplicationContext(), MainActivity.class);
                                startActivity(intentFull);
                                finish();
                            }
                        }, 3000);
                        break;
                    } else if (getDataUsb.equals("2")) {
                        media1.stop();
                        media2.stop();
                        media3.start();
                        Toast.makeText(getApplicationContext(), "ขออภัยการบ้านกล่อง 2 เต็ม", Toast.LENGTH_SHORT).show();
                        final Handler handler = new Handler();
                        handler.postDelayed(new Runnable() {
                            @Override
                            public void run() {
                                Intent intentFull = new Intent(getApplicationContext(), MainActivity.class);
                                startActivity(intentFull);
                                finish();
                            }
                        }, 3000);
                        break;
                    } else if (getDataUsb.equals("3")) {
                        media1.stop();
                        media2.stop();
                        media3.start();
                        Toast.makeText(getApplicationContext(), "ขออภัยการบ้านกล่อง 3 เต็ม", Toast.LENGTH_SHORT).show();
                        final Handler handler = new Handler();
                        handler.postDelayed(new Runnable() {
                            @Override
                            public void run() {
                                Intent intentFull = new Intent(getApplicationContext(), MainActivity.class);
                                startActivity(intentFull);
                                finish();
                            }
                        }, 3000);
                        break;
                    } else if (getDataUsb.equals("4")) {
                        media1.stop();
                        media2.stop();
                        media3.start();
                        Toast.makeText(getApplicationContext(), "ขออภัยการบ้านกล่อง 4 เต็ม", Toast.LENGTH_SHORT).show();
                        final Handler handler = new Handler();
                        handler.postDelayed(new Runnable() {
                            @Override
                            public void run() {
                                Intent intentFull = new Intent(getApplicationContext(), MainActivity.class);
                                startActivity(intentFull);
                                finish();
                            }
                        }, 3000);
                        break;
                    } else {
                        Toast.makeText(getApplicationContext(), "กล่องยังว่าง", Toast.LENGTH_SHORT).show();
                    }
                    break;
                case UsbService.CTS_CHANGE:
                    Toast.makeText(mActivity.get(), "CTS_CHANGE",Toast.LENGTH_LONG).show();
                    break;
                case UsbService.DSR_CHANGE:
                    Toast.makeText(mActivity.get(), "DSR_CHANGE",Toast.LENGTH_LONG).show();
                    break;
            }
        }
    }
    // end get data usb
}