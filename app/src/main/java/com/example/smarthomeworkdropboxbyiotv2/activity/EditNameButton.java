package com.example.smarthomeworkdropboxbyiotv2.activity;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ListView;

import com.example.smarthomeworkdropboxbyiotv2.service.DatabaseStudent;
import com.example.smarthomeworkdropboxbyiotv2.R;

import java.util.ArrayList;

public class EditNameButton extends AppCompatActivity {
    DatabaseStudent mHelper;
    SQLiteDatabase mDb;
    Cursor mCursor;
    ListView listNameButton;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_edit_name_button);

        Button buttonBack = (Button)findViewById(R.id.button_back_main);
        buttonBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intentBack = new Intent(getApplicationContext(), MainActivity.class);
                startActivity(intentBack);
                finish();
            }
        });
    }

    public void onResume() {
        super.onResume();
        mHelper = new DatabaseStudent(this);
        mDb = mHelper.getWritableDatabase();
        mCursor = mDb.rawQuery("SELECT * FROM " + DatabaseStudent.TABLE_NAME_BUTTON, null);

        listNameButton = (ListView)findViewById(R.id.list_name_button);
        listNameButton.setAdapter(updateListView());
        listNameButton.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> arg0, View arg1, int arg2, long arg3) {
                mCursor.moveToPosition(arg2);
                String nameButton = mCursor.getString(mCursor.getColumnIndex(DatabaseStudent.COL_BTNAME));
                Intent intent = new Intent(getApplicationContext(), UpdateNameButton.class);
                intent.putExtra("NAMEBUTTON", nameButton);
                startActivity(intent);
            }
        });
    }

    public void onStop() {
        super.onStop();
        mHelper.close();
        mDb.close();
    }

    public ArrayAdapter<String> updateListView() {
        ArrayList<String> arr_list = new ArrayList<String>();
        mCursor.moveToFirst();
        int i = 1;
        while (!mCursor.isAfterLast()) {
            arr_list.add("Box " + i + " :   " + mCursor.getString(mCursor.getColumnIndex(DatabaseStudent.COL_BTNAME)));
            mCursor.moveToNext();
            i++;
        }
        ArrayAdapter<String> adapterDir = new ArrayAdapter<>(getApplicationContext(),
                R.layout.activity_create, arr_list);
        return adapterDir;
    }
}