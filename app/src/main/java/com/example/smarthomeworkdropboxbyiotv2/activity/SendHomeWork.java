package com.example.smarthomeworkdropboxbyiotv2.activity;

import android.app.ProgressDialog;
import android.content.BroadcastReceiver;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.ServiceConnection;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.media.MediaPlayer;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.IBinder;
import android.view.View;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

import com.example.smarthomeworkdropboxbyiotv2.service.Constant;
import com.example.smarthomeworkdropboxbyiotv2.service.DatabaseStudent;
import com.example.smarthomeworkdropboxbyiotv2.service.HttpParse;
import com.example.smarthomeworkdropboxbyiotv2.R;
import com.example.smarthomeworkdropboxbyiotv2.iotservice.UsbService;
import com.example.smarthomeworkdropboxbyiotv2.utils.BroadcastManager;
import com.example.smarthomeworkdropboxbyiotv2.utils.PermissionManager;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Set;

public class SendHomeWork extends AppCompatActivity {

    // Notifications from UsbService will be received here.
    private final BroadcastManager broadcastManager = new BroadcastManager();
    private final BroadcastReceiver mUsbReceiver = broadcastManager.createBroadcast();
    private final PermissionManager permissionManager = new PermissionManager();

    private UsbService usbService;
    private TextView txToArduino;
    private String showTxt;

    TextView textUserId, textViewSubjectId, textViewBoxId;
    String EmailHolder, finalResult;
    String HttpURL = Constant.URL_BASE + "/SendHomeWork.php";
    ProgressDialog progressDialog;
    HashMap<String, String> hashMap = new HashMap<>();
    HttpParse httpParse = new HttpParse();
    public static final String UserEmail = "";
    MediaPlayer SedBmg0, SedBmg2;

    DatabaseStudent mHelper;
    SQLiteDatabase mDb;
    Cursor mCursor;
    ArrayList<String> arr_list;

    private final ServiceConnection usbConnection = new ServiceConnection() {
        @Override
        public void onServiceConnected(ComponentName arg0, IBinder arg1) {
            usbService = ((UsbService.UsbBinder) arg1).getService();
        }

        @Override
        public void onServiceDisconnected(ComponentName arg0) {
            usbService = null;
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_send_home_work);

        mHelper = new DatabaseStudent(this);
        mDb = mHelper.getWritableDatabase();
        setNameButton();

        SedBmg0 = MediaPlayer.create(SendHomeWork.this, R.raw.snedhomework);
        SedBmg2 = MediaPlayer.create(SendHomeWork.this, R.raw.snedhomework);
        SedBmg0.start();

        txToArduino = (TextView)findViewById(R.id.tx_arduino);

        Bundle bundle = getIntent().getExtras();
        showTxt = bundle.getString("ShowUser");

        textUserId = (TextView)findViewById(R.id.text_user_id);
        textViewSubjectId = (TextView)findViewById(R.id.txt_subject_id);
        textViewBoxId = (TextView)findViewById(R.id.txt_box_id);

        try {
            String userId0 = showTxt.split("-")[2];
            String userId1 = showTxt.split("-")[3];
            String subjectId = showTxt.split("-")[1];

            textUserId.setText(userId0 + "-" + userId1);
            textViewSubjectId.setText(subjectId);

        }catch (Exception e) {
            String userId0 = showTxt.split("-")[2];
            textUserId.setText(userId0);
        }

        Toast.makeText(SendHomeWork.this, "นำการบ้านใส่ที่ช่องใส่การบ้านก่อน", Toast.LENGTH_SHORT).show();

        String box = showTxt.split("-")[0];
        switch (box) {
            case "T1":
                txToArduino.setText("1");
                textViewBoxId.setText(arr_list.get(0));
                break;
            case "T2":
                txToArduino.setText("2");
                textViewBoxId.setText(arr_list.get(1));
                break;
            case "T3":
                txToArduino.setText("3");
                textViewBoxId.setText(arr_list.get(2));
                break;
            case "T4":
                txToArduino.setText("4");
                textViewBoxId.setText(arr_list.get(3));
                break;
        }

        ImageButton imageButtonSpeaker = (ImageButton)findViewById(R.id.img_btn_speaker);
        imageButtonSpeaker.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                SedBmg0.stop();
                SedBmg2.start();
            }
        });

        Button buttonOk = (Button)findViewById(R.id.bt_sead_ok);
        buttonOk.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                SedBmg0.stop();
                if (!txToArduino.getText().toString().equals("")) {
                    String data = txToArduino.getText().toString();
                    if (usbService != null) {
                        usbService.write(data.getBytes());
                        EmailHolder = showTxt;
                        sendHomeWorkFunction(EmailHolder);
                    }
                } else {
                    SedBmg2 = MediaPlayer.create(SendHomeWork.this, R.raw.snedhomework);
                    SedBmg2.start();
                }
            }
        });

        Button buttonBack = (Button)findViewById(R.id.bt_sead_back);
        buttonBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                SedBmg0.stop();
                SedBmg2.stop();
                Intent intentMain = new Intent(SendHomeWork.this, MainActivity.class);
                startActivity(intentMain);
                finish();
            }
        });
    }

    // usb with usb-otg
    @Override
    public void onResume() {
        super.onResume();
        registerReceiver(mUsbReceiver, permissionManager.setFilters());
        startService(UsbService.class, usbConnection, null);
    }

    @Override
    public void onPause() {
        super.onPause();
        unregisterReceiver(mUsbReceiver);
        unbindService(usbConnection);
    }

    private void startService(Class<?> service, ServiceConnection serviceConnection, Bundle extras) {
        if (!UsbService.SERVICE_CONNECTED) {
            Intent startService = new Intent(this, service);
            if (extras != null && !extras.isEmpty()) {
                Set<String> keys = extras.keySet();
                for (String key : keys) {
                    String extra = extras.getString(key);
                    startService.putExtra(key, extra);
                }
            }
            startService(startService);
        }
        Intent bindingIntent = new Intent(this, service);
        bindService(bindingIntent, serviceConnection, Context.BIND_AUTO_CREATE);
    }
    // End use with usb-otg

    public void sendHomeWorkFunction(final String userID) {
        class sendHomeWorkFunctionClass extends AsyncTask<String, Void, String> {
            @Override
            protected void onPreExecute() {
                super.onPreExecute();
                progressDialog = ProgressDialog.show(SendHomeWork.this,"Loading Data",null,true,true);
            }

            @Override
            protected void onPostExecute(String httpResponseMsg) {
                super.onPostExecute(httpResponseMsg);
                progressDialog.dismiss();
                finish();
                Intent intentWait = new Intent(SendHomeWork.this, WaitBackHome.class);
                startActivity(intentWait);
                finish();
            }

            @Override
            protected String doInBackground(String... params) {
                hashMap.put("userID", params[0]);
                finalResult = httpParse.postRequest(hashMap, HttpURL);
                return finalResult;
            }
        }
        sendHomeWorkFunctionClass userLoginClass = new sendHomeWorkFunctionClass();
        userLoginClass.execute(userID);
    }

    public void setNameButton() {
        mCursor = mDb.rawQuery("SELECT * FROM " + DatabaseStudent.TABLE_NAME_BUTTON, null);
        arr_list = new ArrayList<String>();
        mCursor.moveToFirst();
        while (!mCursor.isAfterLast()) {
            String valStd = mCursor.getString(mCursor.getColumnIndex(DatabaseStudent.COL_BTNAME));
            arr_list.add(valStd);
            mCursor.moveToNext();
        }
    }
}