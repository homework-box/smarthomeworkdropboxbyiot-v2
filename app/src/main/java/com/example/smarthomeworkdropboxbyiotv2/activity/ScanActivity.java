package com.example.smarthomeworkdropboxbyiotv2.activity;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.media.MediaPlayer;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import com.example.smarthomeworkdropboxbyiotv2.R;

import me.dm7.barcodescanner.zxing.ZXingScannerView;

public class ScanActivity extends AppCompatActivity implements ZXingScannerView.ResultHandler {
    private ZXingScannerView zXingScannerView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_scan);
        MediaPlayer howToScan = MediaPlayer.create(getApplicationContext(), R.raw.howtoscan);
        howToScan.start();

        scan();
        Button btbBack = (Button)findViewById(R.id.btn_back_home);
        btbBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                howToScan.stop();
                Intent intentBack = new Intent(getApplicationContext(), MainActivity.class);
                startActivity(intentBack);
                finish();
            }
        });
    }

    public void scan() {
        zXingScannerView = findViewById(R.id.cameraaaaa);
        zXingScannerView.setKeepScreenOn(true);
        zXingScannerView.setResultHandler(this);
        zXingScannerView.startCamera();
    }

    @Override
    protected void onPause() {
        super.onPause();
        zXingScannerView.stopCamera();
    }

    @Override
    public void handleResult(com.google.zxing.Result result) {
        Toast.makeText(getApplicationContext(), result.getText(), Toast.LENGTH_SHORT).show();
        zXingScannerView.stopCamera();

        Intent returnIntent = new Intent();
        returnIntent.putExtra("SCAN_RESULT", result.getText());
        setResult(RESULT_OK, returnIntent);
        finish();
    }
}