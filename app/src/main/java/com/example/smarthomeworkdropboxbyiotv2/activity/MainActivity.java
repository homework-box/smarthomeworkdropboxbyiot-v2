package com.example.smarthomeworkdropboxbyiotv2.activity;

import androidx.appcompat.app.AppCompatActivity;

import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.media.MediaPlayer;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.example.smarthomeworkdropboxbyiotv2.service.Constant;
import com.example.smarthomeworkdropboxbyiotv2.service.DatabaseStudent;
import com.example.smarthomeworkdropboxbyiotv2.service.HttpParse;
import com.example.smarthomeworkdropboxbyiotv2.R;

import java.util.HashMap;

public class MainActivity extends AppCompatActivity {

    DatabaseStudent mHelper;
    SQLiteDatabase mDb;
    Cursor mCursor;

    String finalResult;
    String HttpURL = Constant.URL_BASE + "/SendHomeWork.php";
    ProgressDialog progressDialog;
    HashMap<String, String> hashMap = new HashMap<>();
    HttpParse httpParse = new HttpParse();
    public static final String UserEmail = "";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        sendSqlite();

        DatabaseStudent mHelper = new DatabaseStudent(this);
        SQLiteDatabase mDb = mHelper.getWritableDatabase();
        mHelper.close();
        mDb.close();

        isOnline();
        MediaPlayer mpBgm1 = MediaPlayer.create(getApplicationContext(), R.raw.mp1);
        mpBgm1.start();

        Button btOpenIdLogin = (Button)findViewById(R.id.bt_id_student);
        Button btOpenQrcode = (Button)findViewById(R.id.bt_qr_code);

        btOpenIdLogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mpBgm1.stop();
                if (isOnline()) {
                    Intent intent = new Intent(getApplicationContext(), LoginID.class);
                    startActivity(intent);
                    finish();
                } else {
                    Intent intent = new Intent(getApplicationContext(), LoginIdNoInternet.class);
                    startActivity(intent);
                    finish();
                }
            }
        });

        btOpenQrcode.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mpBgm1.stop();
                if (isOnline()) {
                    Intent intent = new Intent(getApplicationContext(), LoginQrcode.class);
                    startActivity(intent);
                    finish();
                } else {
                    Intent intent = new Intent(getApplicationContext(), LoginQrcodeNoInternet.class);
                    startActivity(intent);
                    finish();
                }
            }
        });

        Button buttonOpenEditButton = (Button)findViewById(R.id.button_go_edit_name_button);
        buttonOpenEditButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                final Dialog dialog = new Dialog(MainActivity.this);
                dialog.setTitle("Devahoy");
                dialog.setContentView(R.layout.dialog_custom_login_edit_button);

                final EditText username = (EditText) dialog.findViewById(R.id.edit_username);
                final EditText password = (EditText) dialog.findViewById(R.id.edit_password);
                Button buttonCancel = (Button) dialog.findViewById(R.id.button_cancel);
                Button buttonLogin = (Button) dialog.findViewById(R.id.button_login);

                buttonCancel.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        dialog.dismiss();
                    }
                });

                buttonLogin.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        // Check username password
                        if (username.getText().toString().equals("demo") && password.getText().toString().equals("demo")) {
                            Intent intentGoEditButton = new Intent(getApplicationContext(), EditNameButton.class);
                            startActivity(intentGoEditButton);
                            finish();
                            dialog.dismiss();
                        } else {
                            Toast.makeText(getApplicationContext(), "Login Failed!",
                                    Toast.LENGTH_SHORT).show();
                        }
                    }
                });

                dialog.show();
            }
        });
    }

    // Check Internet
    protected boolean isOnline() {
        ConnectivityManager cm = (ConnectivityManager)getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo netInfo = cm.getActiveNetworkInfo();
        if (netInfo != null && netInfo.isConnectedOrConnecting()) {
            //TextView txtStatus = (TextView)findViewById(R.id.statusWifi);
            //txtStatus.setText("Wifi Connect");
            return true;
        } else {
            //TextView txtStatus = (TextView)findViewById(R.id.statusWifi);
            //txtStatus.setText("Wifi Not Connect");
            return false;
        }
    }

    public void sendSqlite() {
        //isOnline();
        if (isOnline()) {
            mHelper = new DatabaseStudent(this);
            mDb = mHelper.getReadableDatabase();
            mCursor = mDb.rawQuery("SELECT * FROM " + DatabaseStudent.TABLE_NAME, null);

            mCursor.moveToFirst();
            while (!mCursor.isAfterLast()) {
                String valId = mCursor.getString(mCursor.getColumnIndex(DatabaseStudent.COL_KEYID));
                String valStd = mCursor.getString(mCursor.getColumnIndex(DatabaseStudent.COL_IDSTD));
                sendHomeWorkFunction(valStd);
                Toast.makeText(getApplicationContext(), valId + " : " + valStd, Toast.LENGTH_SHORT).show();
                // ลบข้อมูลออกจากตาราง
                mDb.execSQL("DELETE FROM " + DatabaseStudent.TABLE_NAME + " WHERE " + DatabaseStudent.COL_IDSTD + "='" + valStd + "';");
                //mCursor.requery();
                Toast.makeText(getApplicationContext(), "ลบ", Toast.LENGTH_SHORT).show();
                mCursor.moveToNext(); //เลื่อน Cursor ไปแถวถันไป
            }

        } else {
            Toast.makeText(MainActivity.this, "db ว่าง", Toast.LENGTH_SHORT).show();
        }

    }

    public void sendHomeWorkFunction(final String userID) {
        class sendHomeWorkFunctionClass extends AsyncTask<String, Void, String> {
            @Override
            protected void onPreExecute() {
                super.onPreExecute();
                //progressDialog = ProgressDialog.show(MainActivity.this,"Loading Data",null,true,true);
            }

            @Override
            protected void onPostExecute(String httpResponseMsg) {
                super.onPostExecute(httpResponseMsg);
                //progressDialog.dismiss();
                //finish();
            }

            @Override
            protected String doInBackground(String... params) {
                hashMap.put("userID", params[0]);
                finalResult = httpParse.postRequest(hashMap, HttpURL);
                return finalResult;
            }
        }
        sendHomeWorkFunctionClass userLoginClass = new sendHomeWorkFunctionClass();
        userLoginClass.execute(userID);
    }
}