package com.example.smarthomeworkdropboxbyiotv2.activity;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.media.MediaPlayer;
import android.os.Bundle;
import android.widget.TextView;

import com.example.smarthomeworkdropboxbyiotv2.R;

import java.util.Timer;
import java.util.TimerTask;

public class WaitBackHome extends AppCompatActivity {

    private TextView textViewfinish;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_wait_back_home);

        final MediaPlayer WbhBgm1 = MediaPlayer.create(getApplicationContext(), R.raw.wbp1);
        WbhBgm1.start();

        Timer timer1 = new Timer();
        timer1.schedule(new TimerTask() {
            @Override
            public void run() {
                WbhBgm1.stop();
                Intent intent = new Intent(getApplicationContext(), FinishOk.class);
                startActivity(intent);
                finish();
            }
        }, 30000);
    }
}