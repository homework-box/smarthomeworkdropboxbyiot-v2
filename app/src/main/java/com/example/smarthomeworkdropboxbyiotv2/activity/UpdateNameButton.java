package com.example.smarthomeworkdropboxbyiotv2.activity;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.example.smarthomeworkdropboxbyiotv2.service.DatabaseStudent;
import com.example.smarthomeworkdropboxbyiotv2.R;

public class UpdateNameButton extends AppCompatActivity {
    DatabaseStudent mHelper;
    SQLiteDatabase mDb;
    Cursor mCursor;
    String namebutton;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_update_name_button);

        namebutton = getIntent().getExtras().getString("NAMEBUTTON");
        mHelper = new DatabaseStudent(this);
        mDb = mHelper.getWritableDatabase();
        mCursor = mDb.rawQuery("SELECT * FROM " + DatabaseStudent.TABLE_NAME_BUTTON + " WHERE "
                + DatabaseStudent.COL_BTNAME + "='" + namebutton + "'", null);

        final EditText editNameButton = (EditText)findViewById(R.id.edit_name_button_update);
        editNameButton.setText(namebutton);

        Button buttonUpdate = (Button)findViewById(R.id.button_update);
        buttonUpdate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String nameUpdate = editNameButton.getText().toString();
                if (namebutton.length() != 0) {
                    mDb.execSQL("UPDATE " + DatabaseStudent.TABLE_NAME_BUTTON + " SET "
                            + DatabaseStudent.COL_BTNAME + "='" + nameUpdate + "' WHERE "
                            + DatabaseStudent.COL_BTNAME + "='" + namebutton + "';");
                    Toast.makeText(getApplicationContext(), "แก้ไขข้อมูลนักเรียนเรียบร้อยแล้ว", Toast.LENGTH_SHORT).show();
                    finish();
                } else {
                    Toast.makeText(getApplicationContext(), "กรุณาใส่ข้อมูลนักเรียนให้ครบทุกช่อง", Toast.LENGTH_SHORT).show();
                }
            }
        });

        Button buttonNameClear = (Button)findViewById(R.id.button_name_clear);
        buttonNameClear.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                editNameButton.setText("");
            }
        });

        Button buttonBackEditName = (Button)findViewById(R.id.btn_back_edit_name);
        buttonBackEditName.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intentGoEdit = new Intent(getApplicationContext(), EditNameButton.class);
                startActivity(intentGoEdit);
                finish();
            }
        });
    }

    public void onStop() {
        super.onStop();
        mHelper.close();
        mDb.close();
    }
}