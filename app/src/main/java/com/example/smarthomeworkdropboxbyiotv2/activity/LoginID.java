package com.example.smarthomeworkdropboxbyiotv2.activity;

import androidx.appcompat.app.AppCompatActivity;

import android.app.ProgressDialog;
import android.content.BroadcastReceiver;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.ServiceConnection;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.media.MediaPlayer;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.os.IBinder;
import android.os.Message;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.RadioButton;
import android.widget.TextView;
import android.widget.Toast;

import com.example.smarthomeworkdropboxbyiotv2.service.Constant;
import com.example.smarthomeworkdropboxbyiotv2.service.DatabaseStudent;
import com.example.smarthomeworkdropboxbyiotv2.service.HttpParse;
import com.example.smarthomeworkdropboxbyiotv2.R;
import com.example.smarthomeworkdropboxbyiotv2.iotservice.UsbService;
import com.example.smarthomeworkdropboxbyiotv2.utils.BroadcastManager;
import com.example.smarthomeworkdropboxbyiotv2.utils.PermissionManager;

import java.lang.ref.WeakReference;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Set;
import java.util.Timer;
import java.util.TimerTask;

public class LoginID extends AppCompatActivity {

    // Notifications from UsbService will be received here.
    private final BroadcastManager broadcastManager = new BroadcastManager();
    private final BroadcastReceiver mUsbReceiver = broadcastManager.createBroadcast();
    private final PermissionManager permissionManager = new PermissionManager();

    private UsbService usbService;
    TextView textViewUsb;

    // get serial port
    private MyHandler mHandler;
    String getDataUsb;

    EditText editTextId;
    String EmailHolder, finalResult;
    String HttpURL = Constant.URL_BASE + "/LoginHomeWork.php";
    Boolean checkEditText;
    ProgressDialog progressDialog;
    HashMap<String, String> hashMap = new HashMap<>();
    HttpParse httpParse = new HttpParse();
    public static final String UserEmail = "";

    EditText editTextIdSubject;
    RadioButton radioButtonTh1;
    RadioButton radioButtonTh2;
    RadioButton radioButtonTh3;
    RadioButton radioButtonTh4;
    String radioButtonSend;
    TextView textViewTestPasswd;

    // set name select send homework
    DatabaseStudent mHelper;
    SQLiteDatabase mDb;
    Cursor mCursor;
    String box1, box2, box3, box4;

    MediaPlayer media1, media2, media3;

    private final ServiceConnection usbConnection = new ServiceConnection() {
        @Override
        public void onServiceConnected(ComponentName arg0, IBinder arg1) {
            usbService = ((UsbService.UsbBinder) arg1).getService();
            usbService.setHandler(mHandler); // get serial port
        }

        @Override
        public void onServiceDisconnected(ComponentName arg0) {
            usbService = null;
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login_i_d);

        mHandler = new MyHandler(this); // get serial port

        mHelper = new DatabaseStudent(this);
        mDb = mHelper.getWritableDatabase();
        setNameButton();

        media1 = MediaPlayer.create(LoginID.this, R.raw.homeloginid);
        media2 = MediaPlayer.create(LoginID.this, R.raw.homeloginid);
        media3 = MediaPlayer.create(LoginID.this, R.raw.boxfull);
        media1.start();

        editTextId = (EditText)findViewById(R.id.edit_id_code);
        Button buttonLogin = (Button)findViewById(R.id.bt_id_login);
        Button buttonBack = (Button)findViewById(R.id.bt_back_main);
        textViewUsb = (TextView)findViewById(R.id.text_view_usb);

        editTextIdSubject = (EditText)findViewById(R.id.edit_id_subject);
        radioButtonTh1 = (RadioButton)findViewById(R.id.radio_th_1);
        radioButtonTh2 = (RadioButton)findViewById(R.id.radio_th_2);
        radioButtonTh3 = (RadioButton)findViewById(R.id.radio_th_3);
        radioButtonTh4 = (RadioButton)findViewById(R.id.radio_th_4);
        textViewTestPasswd = (TextView)findViewById(R.id.txt_test_passwd);

        editTextId.requestFocus();

        radioButtonTh1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                radioButtonSend = "T1";
                // send usb Data
                textViewUsb.setText("6");
                if (!textViewUsb.getText().toString().equals("")) {
                    String data = textViewUsb.getText().toString();
                    // if UsbService was correctly binded, Send data
                    if (usbService != null) {
                        usbService.write(data.getBytes());
                    }
                }
                // end
            }
        });

        radioButtonTh2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                radioButtonSend = "T2";
                // send usb Data
                textViewUsb.setText("7");
                if (!textViewUsb.getText().toString().equals("")) {
                    String data = textViewUsb.getText().toString();
                    // if UsbService was correctly binded, Send data
                    if (usbService != null) {
                        usbService.write(data.getBytes());
                    }
                }
                // end
            }
        });

        radioButtonTh3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                radioButtonSend = "T3";
                // send usb Data
                textViewUsb.setText("8");
                if (!textViewUsb.getText().toString().equals("")) {
                    String data = textViewUsb.getText().toString();
                    // if UsbService was correctly binded, Send data
                    if (usbService != null) {
                        usbService.write(data.getBytes());
                    }
                }
                // end
            }
        });

        radioButtonTh4.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                radioButtonSend = "T4";
                // send usb Data
                textViewUsb.setText("9");
                if (!textViewUsb.getText().toString().equals("")) {
                    String data = textViewUsb.getText().toString();
                    // if UsbService was correctly binded, Send data
                    if (usbService != null) {
                        usbService.write(data.getBytes());
                    }
                }
                // end
            }
        });

        ImageButton imageButtonSpeaker = (ImageButton)findViewById(R.id.img_btn_speaker);
        imageButtonSpeaker.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                media1.stop();
                media2.start();
            }
        });

        buttonLogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                media1.stop();
                media2.stop();
                checkEditTextIsEmptyOrNot();
                if (checkEditText) {
                    userLoginFunction(EmailHolder);
                } else {
                    MediaPlayer noPassWd = MediaPlayer.create(getApplicationContext(), R.raw.nopasswd);
                    noPassWd.start();
                }
            }
        });

        buttonBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                media1.stop();
                media2.stop();
                Intent intentMain = new Intent(getApplicationContext(), MainActivity.class);
                startActivity(intentMain);
                finish();
            }
        });
    }

    // เช็ตว่าช่องใส่ข้อมูลว่างหรือไม่
    public void checkEditTextIsEmptyOrNot() {
        String id1, id2, id3, id4;
        id1 = editTextId.getText().toString();
        id2 = editTextIdSubject.getText().toString();
        id3 = radioButtonSend + "-" + id2 + "-" + id1;
        EmailHolder = id3;
        //Toast.makeText(getApplicationContext(), "check in " + EmailHolder ,Toast.LENGTH_SHORT).show();
        if (TextUtils.isEmpty(EmailHolder)) {
            checkEditText = false;
        } else {
            checkEditText = true;
        }
    }

    public void userLoginFunction(final String email) {
        // เราก็จะใช้ AsyncTask ช่วยในการคำนวณ ประมวลผล โดยเมื่อ AsyncTask ทำงานอยู่ มันจะไม่กระทบกับหน้าจอ UI ของเรา
        class UserLoginFunctionClass extends AsyncTask<String, Void, String> {

            // ลำดับการทำงานที่ 1
            @Override
            protected void onPreExecute() {
                super.onPreExecute();
                progressDialog = ProgressDialog.show(LoginID.this, "Loading Data", null, true, true);
            }

            // ลำดับการทำงานที่ 3 ทำหลังจาก doInBackground ทำงานเสร็จลง
            @Override
            protected void onPostExecute(String httpResponseMsg) {
                super.onPostExecute(httpResponseMsg);
                progressDialog.dismiss();

                if (httpResponseMsg.equalsIgnoreCase("Data Matched")) {
                    finish();
                    Intent intent = new Intent(LoginID.this, SendHomeWork.class);
                    intent.putExtra("ShowUser", EmailHolder);

                    // send usb Data
                    textViewUsb.setText("5");
                    if (!textViewUsb.getText().toString().equals("")) {
                        String data = textViewUsb.getText().toString();
                        // if UsbService was correctly binded, Send data
                        if (usbService != null) {
                            usbService.write(data.getBytes());
                        }
                    }
                    // end
                    startActivity(intent);
                    finish();
                } else {
                    Toast.makeText(LoginID.this, httpResponseMsg.toString(), Toast.LENGTH_LONG).show();
                    MediaPlayer LgBmg = MediaPlayer.create(LoginID.this, R.raw.lgp1);
                    LgBmg.start();
                }
            }

            // ลำดับการทำงานที่ 2
            @Override
            protected String doInBackground(String... params) {
                Log.d("XXXXXXXX", "Connecting to " + HttpURL + " with " + email);
                hashMap.put("email", params[0]);
                finalResult = httpParse.postRequest(hashMap, HttpURL);
                return finalResult;
            }
        }

        UserLoginFunctionClass userLoginClass = new UserLoginFunctionClass();
        userLoginClass.execute(email);
    }

    // send usb data
    @Override
    public void onResume() {
        super.onResume();
        registerReceiver(mUsbReceiver, permissionManager.setFilters());
        startService(UsbService.class, usbConnection, null);
    }

    @Override
    public void onPause() {
        super.onPause();
        unregisterReceiver(mUsbReceiver);
        unbindService(usbConnection);
    }

    private void startService(Class<?> service, ServiceConnection serviceConnection, Bundle extras) {
        if (!UsbService.SERVICE_CONNECTED) {
            Intent startService = new Intent(this, service);
            if (extras != null && !extras.isEmpty()) {
                Set<String> keys = extras.keySet();
                for (String key : keys) {
                    String extra = extras.getString(key);
                    startService.putExtra(key, extra);
                }
            }
            startService(startService);
        }
        Intent bindingIntent = new Intent(this, service);
        bindService(bindingIntent, serviceConnection, Context.BIND_AUTO_CREATE);
    }
    // end send usb data

    public void setNameButton() {
        mCursor = mDb.rawQuery("SELECT * FROM " + DatabaseStudent.TABLE_NAME_BUTTON, null);
        ArrayList<String> arr_list = new ArrayList<String>();
        mCursor.moveToFirst();
        while (!mCursor.isAfterLast()) {
            String valStd = mCursor.getString(mCursor.getColumnIndex(DatabaseStudent.COL_BTNAME));
            arr_list.add(valStd);
            mCursor.moveToNext();
        }

        radioButtonTh1 = (RadioButton)findViewById(R.id.radio_th_1);
        radioButtonTh2 = (RadioButton)findViewById(R.id.radio_th_2);
        radioButtonTh3 = (RadioButton)findViewById(R.id.radio_th_3);
        radioButtonTh4 = (RadioButton)findViewById(R.id.radio_th_4);

        try {
            radioButtonTh1.setText(arr_list.get(0));
            radioButtonTh2.setText(arr_list.get(1));
            radioButtonTh3.setText(arr_list.get(2));
            radioButtonTh4.setText(arr_list.get(3));
        } catch (Exception e) {
            Toast.makeText(getApplicationContext(), "ไม่มีชื่อ Button ใน DB", Toast.LENGTH_SHORT).show();
        }
    }

    // get data usb
    private class MyHandler extends Handler {
        private final WeakReference<LoginID> mActivity;

        public MyHandler(LoginID activity) {
            mActivity = new WeakReference<>(activity);
        }

        @Override
        public void handleMessage(Message msg) {
            switch (msg.what) {
                case UsbService.MESSAGE_FROM_SERIAL_PORT:
                    String data = (String) msg.obj;
                    getDataUsb = data; // get data usb to getDataUsb
                    //Toast.makeText(getApplicationContext(), getDataUsb, Toast.LENGTH_SHORT).show();
                    if (getDataUsb.equals("1")) {
                        media1.stop();
                        media2.stop();
                        media3.start();
                        Toast.makeText(getApplicationContext(), "ขออภัยการบ้านกล่อง 1 เต็ม", Toast.LENGTH_SHORT).show();
                        final Handler handler = new Handler();
                        handler.postDelayed(new Runnable() {
                            @Override
                            public void run() {
                                Intent intentFull = new Intent(getApplicationContext(), MainActivity.class);
                                startActivity(intentFull);
                                finish();
                            }
                        }, 3000);
                        break;
                    } else if (getDataUsb.equals("2")) {
                        media1.stop();
                        media2.stop();
                        media3.start();
                        Toast.makeText(getApplicationContext(), "ขออภัยการบ้านกล่อง 2 เต็ม", Toast.LENGTH_SHORT).show();
                        final Handler handler = new Handler();
                        handler.postDelayed(new Runnable() {
                            @Override
                            public void run() {
                                Intent intentFull = new Intent(getApplicationContext(), MainActivity.class);
                                startActivity(intentFull);
                                finish();
                            }
                        }, 3000);
                        break;
                    } else if (getDataUsb.equals("3")) {
                        media1.stop();
                        media2.stop();
                        media3.start();
                        Toast.makeText(getApplicationContext(), "ขออภัยการบ้านกล่อง 3 เต็ม", Toast.LENGTH_SHORT).show();
                        final Handler handler = new Handler();
                        handler.postDelayed(new Runnable() {
                            @Override
                            public void run() {
                                Intent intentFull = new Intent(getApplicationContext(), MainActivity.class);
                                startActivity(intentFull);
                                finish();
                            }
                        }, 3000);
                        break;
                    } else if (getDataUsb.equals("4")) {
                        media1.stop();
                        media2.stop();
                        media3.start();
                        Toast.makeText(getApplicationContext(), "ขออภัยการบ้านกล่อง 4 เต็ม", Toast.LENGTH_SHORT).show();
                        final Handler handler = new Handler();
                        handler.postDelayed(new Runnable() {
                            @Override
                            public void run() {
                                Intent intentFull = new Intent(getApplicationContext(), MainActivity.class);
                                startActivity(intentFull);
                                finish();
                            }
                        }, 3000);
                        break;
                    } else {
                        //Toast.makeText(getApplicationContext(), "กล่องยังว่าง", Toast.LENGTH_SHORT).show();
                    }
                    break;
                case UsbService.CTS_CHANGE:
                    Toast.makeText(mActivity.get(), "CTS_CHANGE",Toast.LENGTH_LONG).show();
                    break;
                case UsbService.DSR_CHANGE:
                    Toast.makeText(mActivity.get(), "DSR_CHANGE",Toast.LENGTH_LONG).show();
                    break;
            }
        }
    }
    // end get data usb
}