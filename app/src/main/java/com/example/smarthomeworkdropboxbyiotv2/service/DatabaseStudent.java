package com.example.smarthomeworkdropboxbyiotv2.service;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

public class DatabaseStudent extends SQLiteOpenHelper {

    // All Static variables
    // Database Version
    private static final int DB_VERSION = 1;
    // Database Name
    private static final String DB_NAME = "MyDBHomeWork";

    // Table data get id when can't connect internet
    // Contacts table name
    public static final String TABLE_NAME = "IdStudent";
    // Contacts Table Columns name
    public static final String COL_KEYID = "_id";
    public static final String COL_IDSTD = "id_student";
    // end table can't connect internet

    // Table data name button
    public static final String TABLE_NAME_BUTTON = "NameButton";

    public static final String COL_BTID = "_id";
    public static final String COL_BTNAME = "name_button";
    // end table name button

    private static final String IdStudentSQL = "CREATE TABLE " + TABLE_NAME + "("
            + COL_KEYID + " INTEGER PRIMARY KEY," + COL_IDSTD + " TEXT" + ");";

    private static final String NameButtonSQL = "CREATE TABLE " + TABLE_NAME_BUTTON + "("
            + COL_BTID + " INTEGER PRIMARY KEY," + COL_BTNAME + " TEXT" + ");";

    private static final String InsertNameButton1 = "INSERT INTO " + TABLE_NAME_BUTTON + " ("
            + COL_BTNAME + ") VALUES ('Th 1');";
    private static final String InsertNameButton2 = "INSERT INTO " + TABLE_NAME_BUTTON + " ("
            + COL_BTNAME + ") VALUES ('Th 2');";
    private static final String InsertNameButton3 = "INSERT INTO " + TABLE_NAME_BUTTON + " ("
            + COL_BTNAME + ") VALUES ('Th 3');";
    private static final String InsertNameButton4 = "INSERT INTO " + TABLE_NAME_BUTTON + " ("
            + COL_BTNAME + ") VALUES ('Th 4');";



    public DatabaseStudent(Context context) {
        super(context, DB_NAME, null, DB_VERSION);
    }

    // Creating Tables
    public void onCreate(SQLiteDatabase db) {
        db.execSQL(IdStudentSQL);
        db.execSQL(NameButtonSQL);
        db.execSQL(InsertNameButton1);
        db.execSQL(InsertNameButton2);
        db.execSQL(InsertNameButton3);
        db.execSQL(InsertNameButton4);
        Log.d("CREATE Multiple Table", "Success");
    }

    // Upgrading database
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_NAME);
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_NAME_BUTTON);
        onCreate(db);
    }
}
